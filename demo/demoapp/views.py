from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.generic import TemplateView

# Create your views here.

'''

def home(request):
    return render_to_response('home.html')
'''

class Home(TemplateView):
    template_name = 'home.html'